// Copyright 2021 Tony Butterfield
// MIT Licence
// for more information see https://gitlab.com/tonbut/logic-pro-scripter-scripts
var inTonic = 0
var inMode = 0
var outTonic = 0
var outMode = 0
var roundMode = 0

var majorScale = [0, 2, 4, 5, 7, 9, 11, 12]
var scaleDegrees = [0, 0.5, 1, 1.5, 2, 3, 3.5, 4, 4.5, 5, 5.5, 6]
var noteNames = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B'
]

function HandleMIDI (event) {
  if (event instanceof NoteOn) {
    var pitchIn = event.pitch
    var pitchOut = transpose(pitchIn, true)
    var e = new NoteOn()
    e.pitch = pitchOut
    e.velocity = event.velocity
    e.send()
  } else if (event instanceof NoteOff) {
    var pitchIn = event.pitch
    var pitchOut = transpose(pitchIn, false)
    var e = new NoteOff()
    e.pitch = pitchOut
    e.velocity = 0
    e.send()
  } else {
    event.send()
  }
}

function transpose (pitch, debug) {
  
  var note = pitch - inTonic
  var noteInScale = note % 12
  var octave = (note - noteInScale) / 12
  note -= octave * 12

  //lookup scale degree
  var modeOffset = majorScale[inMode]
  var scaleDegree =
    scaleDegrees[(modeOffset + noteInScale) % 12] - scaleDegrees[modeOffset]
  scaleDegree = (scaleDegree + 7) % 7

  //round if needed
  var roundedDegree = Math.floor(scaleDegree)
  var rounded = roundedDegree != scaleDegree

  //now transpose to new scale
  var outOffset = roundedDegree + outMode
  outOffset %= 7
  var outPitch = majorScale[outOffset]
  var failed = false
  if (rounded) {
    //see if we can find a note between
    if (outPitch + 1 < majorScale[outOffset + 1]) {
      outPitch += 1
    } else {
      failed = true
      if (roundMode != 0) {
        outPitch = majorScale[outOffset + 1]
      }
    }
  }

  outPitch += 12 * octave
  outPitch += outTonic - majorScale[outMode]

  if (outPitch - pitch < -6) {
    outPitch += 12
  }
  if (outPitch - pitch < -6) {
    outPitch += 12
  }
  if (outPitch - pitch > 6) {
    outPitch -= 12
  }

  if (debug) {
    var msg = ''
    if (outPitch != pitch) msg += '* '
    if (failed) msg += 'rounded'
    var n1 = noteNames[pitch % 12]
    var n2 = noteNames[outPitch % 12]
    Trace(n1 + ' -> ' + n2 + ' ' + msg + ' ' + (outPitch - pitch))
  }

  return outPitch
}

function ParameterChanged (param, value) {
  switch (param) {
    case 1:
      inTonic = value
      break
    case 2:
      inMode = value
      break
    case 4:
      outTonic = value
      break
    case 5:
      outMode = value
      break
    case 6:
      roundMode = value
      break
  }
}

var PluginParameters = [
  {
    name: '------ Input Scale ------',
    type: 'text'
  },
  {
    name: 'Tonic',
    type: 'menu',
    valueStrings: [
      'C',
      'C#',
      'D',
      'D#',
      'E',
      'F',
      'F#',
      'G',
      'G#',
      'A',
      'A#',
      'B'
    ],
    numberOfSteps: 12,
    defaultValue: 0
  },
  {
    name: 'Mode',
    type: 'menu',
    valueStrings: [
      'Ionian (Major)',
      'Dorian',
      'Phyrgian',
      'Lydian',
      'Mixolydian',
      'Aeolian (Minor)',
      'Locrian'
    ],
    numberOfSteps: 7,
    defaultValue: 0
  },
  {
    name: '------ Output Scale ------',
    type: 'text'
  },
  {
    name: 'Tonic 2',
    type: 'menu',
    valueStrings: [
      'C',
      'C#',
      'D',
      'D#',
      'E',
      'F',
      'F#',
      'G',
      'G#',
      'A',
      'A#',
      'B'
    ],
    numberOfSteps: 12,
    defaultValue: 0
  },
  {
    name: 'Mode 2',
    type: 'menu',
    valueStrings: [
      'Ionian (Major)',
      'Dorian',
      'Phyrgian',
      'Lydian',
      'Mixolydian',
      'Aeolian (Minor)',
      'Locrian'
    ],
    numberOfSteps: 7,
    defaultValue: 0
  },
  {
    name: 'Round',
    type: 'menu',
    valueStrings: ['down', 'up'],
    numberOfSteps: 2,
    defaultValue: 0
  }
]

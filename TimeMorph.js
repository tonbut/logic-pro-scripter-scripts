var NeedsTimingInfo = true

function HandleMIDI (event) {

  if (event instanceof NoteOn) {
    var pitchIn = event.pitch
    var time = event.beatPos

    if (pitchIn == triggerNote) {
      setupTrigger(time)
    } else {
      if (time < triggerEnd - 0.01 && time >= t0 - 0.01) {
        var timeOut = mapTime(time)
        Trace('On at ' + formatTime(time) + ' ' + formatTime(timeOut))
        sendDelayed(event, timeOut)
      } else {
        event.send()
      }
    }
  } else if (event instanceof NoteOff) {
    var time = event.beatPos
    if (time < triggerEnd - 0.01 && time >= t0 - 0.01) {
      var timeOut = mapTime(time)
      Trace('Off at ' + formatTime(time) + ' ' + formatTime(timeOut))
      sendDelayed(event, timeOut)
    } else {
      event.send()
    }
  } else {
    event.send()
  }
}

function sendDelayed (event, time) {
  var delay = time - event.beatPos
  if (delay >= 0) {
    var ms = delay * delayMultiplier
    event.sendAfterMilliseconds(ms)
  } else {
    event.send()
  }
}

function mapTime2 (t) {
  var tdiff = t - inStart

  return tdiff + outStart
}

function mapTime (t) {
  var tdiff = t - inStart
  var a = 0.5 * acc
  var b = 1
  var c = -tdiff
  var outTime = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a)
  outTime += outStart

  return outTime
}

var t0 = 0
var triggerEnd = 0
var inDuration = 0
var outDuration = 0
var inStart = 0
var outStart = 0
var acc
var delayMultiplier
function setupTrigger (time) {
  Trace('triggered at ' + formatTime(time))
  t0 = time
  triggerEnd = time + endOffset
  inDuration = duration
  outDuration = (2 * duration) / (1 + tempoMultiplier)
  inStart = t0
  acc = (tempoMultiplier - 1) / outDuration
  if (tempoMultiplier > 1) {
    outStart = inStart + (inDuration - outDuration)
  } else {
    outStart = inStart
  }
  delayMultiplier = 60000 / GetTimingInfo().tempo
  Trace('inDuration=' + inDuration)
  Trace('outDuration=' + outDuration)
  Trace('inStart=' + formatTime(inStart))
  Trace('outStart=' + formatTime(outStart))
  Trace('end=' + formatTime(triggerEnd))
}

function formatTime (time) {
  return time.toFixed(2)
}

var triggerNote
var endOffset
var duration
var tempoMultiplier

function ParameterChanged (param, value) {
  switch (param) {
    case 0:
      triggerNote = value
      break
    case 1:
      endOffset = value
      break
    case 2:
      duration = value
      break
    case 3:
      tempoMultiplier = value
      break
  }
}

var PluginParameters = [
  {
    name: 'Trigger Note',
    type: 'lin',
    numberOfSteps: 36,
    minValue: 0,
    maxValue: 36,
    defaultValue: 0
  },
  {
    name: 'End offset (beats)',
    type: 'lin',
    numberOfSteps: 32,
    minValue: 0,
    maxValue: 32,
    defaultValue: 16
  },
  {
    name: 'Duration to scale (beats)',
    type: 'lin',
    numberOfSteps: 16,
    minValue: 0,
    maxValue: 16,
    defaultValue: 4
  },
  {
    name: 'Tempo multiplier',
    type: 'log',
    numberOfSteps: 100,
    minValue: 0.25,
    maxValue: 4,
    defaultValue: 1
  }
]

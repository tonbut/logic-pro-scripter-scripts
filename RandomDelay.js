// Copyright 2021 Tony Butterfield
// MIT Licence
// for more information see https://gitlab.com/tonbut/logic-pro-scripter-scripts
// https://www.logicprohelp.com/forum/viewtopic.php?t=121684

function HandleMIDI (event) {
  if (event instanceof NoteOn) {
    event.sendAfterMilliseconds(randomDelay(delayMin, delayMax))
  } else {
    event.send()
  }
}

var delayMin = 0
var delayMax = 0
var PluginParameters = [
  {
    name: 'Delay Min',
    type: 'lin',
    unit: 'ms',
    minValue: 0,
    maxValue: 100,
    defaultValue: 1,
    numberOfSteps: 40
  },
  {
    name: 'Delay Max',
    type: 'lin',
    unit: 'ms',
    minValue: 0,
    maxValue: 100,
    defaultValue: 1,
    numberOfSteps: 40
  }
]

function ParameterChanged (param, value) {
  if (param == 0) delayMin = value
  if (param == 1) delayMax = value
}

function randomDelay (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

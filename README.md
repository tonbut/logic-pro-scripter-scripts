# Logic Pro Scripter Scripts

A collection of Javascript scripts for Apple Logic Pro Scripter MIDIFX plugin

## BBC Envelope

BBC recreates the retro 8-bit envelope sound control approach. This script works by breaking down each input note into a sequence of sub-notes in similar way to an arpeggiator.  Both volume and pitch are controlled. Volume control supports simple ADSR. Pitch control supports up to 8 groups of sequenced pitch changes.

Let's look at more detail of the controls. The first thing to understand is the master clock which drives all pitch and volume transitions. This is controlled in the Mod Wheel Speed section. Each beat is sub-divided up into a number of steps. Initially this sub-division will be the the Beat division Min value. I.e. if this value is 8, then there will be 8 steps in each beat. Increasing the modwheel will ramp the number of steps up to Beat Division Max. Obviously if both values are equal the modwheel will have no effect. The Quantive to ^2 if set to yes will force the beat divisions to be powers of 2, i.e. 4,8,16,32,64 etc. This keeps the divisions synced to the beat. If set to no then the variation in speed will be smoothly controlled by the modwheel.

As mentioned above pitch is controlled in groups of pitch changes in the Pitch Pattern section. It is possible to have between 0 and 16 groups. Each group consists of a pitch change which is in ratios of a semi-tone. Each pitch change can be repeated between 1 and 64 times. When all the steps have been completed in one group the next group is processed. If a group has steps set to 0 the group is skipped. When the last group has been completed processing moves back to the first group in a circular fashion.

Further control of pitch processing can be configured in the Pitch Control section. Low Octave and Octave Range control the range of pitches that can be output from the  envelope. Octaves start on C and end on B. What happens when the pitch groups cause the pitch to go out of range is controlled by the Out-of-range parameter. The Silent option causes pitches out of range to not be played. The Stop option causes the envelope to terminate.  The Wrap option causes the pitch to wrap around low to high and high to low. The wrap option emulates the original BBC envelope behaviour. 

The default Semitone subdivision of 1 causes all increments and decrements in pitch to be multiples of a semitone. Setting it to either 2 or 4 creates either 2 or 4 micro tonal steps between the semi-tones. This creates a smoother pitch sweep where needed but requires pitch bend to be configured in the track instrument. Depending upon the instrument it is not always possible to get smooth effects when using this.

The Advanced Features section contains two parameters that control the modification of pitch groups over time. Pitch Step Differential controls how the number of semitones of the first 3 pitch groups changes each time the group is processed. By default this parameter is 0 and no change occurs. Setting it to a positive values causes the first and third groups to grow larger and the second step to grow smaller by an equal amount. Negative values cause the  inverse. The parameter is useful for creating steadily increasing vibratos or sweeps. 

The Time Step Differential parameter controls how the number of steps in the first 3 pitch groups change over time.  Setting a positive value will cause the number of steps to increase causing increasingly longer time spent on that group. The can lead to increasing ranges of movement that appear to slow with time.

Volume envelopes rely on the instrument on the track supporting velocity sensitivity. Pitch can work in semi-tone intervals and semi-tone sub-divisions. To achieve semi-tone sub-divisions  pitch bend is used. 

Important notes:
- To get volume envelopes your track instrument must have full range velocity control, i.e. velocities of 1 to 127 go from quietest to full volume.
- Pitch bend is used to control sub semitone subdivisions (if enabled) This features assumes the pitch-bend range on the track instrument is set to +/- 1 semi-tone. For the pitches to be correct you must set this.
- In my experience you need to tweak the attack and release of notes on the track instrument to a small but non-zero value to get a smooth transition between notes. If you can set the oscillator not to reset phase this is also beneficial to a smooth sound.

## Mode Transposer

This script can transpose midi notes from one key to another and also transpose to a different mode. Transposing mode is more complex than simply changing key. Changing key is simply adding a fixed offset to each note in the input. Changing mode requires matching each note to its position in the input scale and then selecting the equivalent note in the output scale. Non diatonic notes from the input scale require special handling.  Because they have no well defined position in the input scale we simply map them to a the place on the destination scale between valid notes. If there is no note in the destination scale between valid notes then the note is mapped up or down to a valid not depending upon the Round parameter.

The only other place i have seen this kind of algorithm is in hooktheory.com.

It is possible to get create and bizarre interpretations of a scale by incorrectly setting the input scale. Sometimes this can lead to something enjoyably surprising, most often it sounds horrible.

## Time Morph

This script causes a smooth linear interpolation between two tempos on a MIDI track. It can cause a controlled speed up or slowdown that if done carefully can start and end synced to the beat. So for example you can create an arpeggio that starts in 1/8 timing and finishes in 1/16 timing with both ends synchronized to the beat.

Multiple transitions can be placed on one track because the transition is initiated by a trigger note that can be set on a low register. Once triggered notes are captured, buffered and released after a variable delay to create the appropriate effect. Once the transition has completed the script acts transparently again other than listening for the next trigger note.

**Trigger Note** - the MIDI note that will initiate a transition. This note will never be passed through. For reference MIDI note 0 is C-1, 12 is C0.

 **End offset** - the duration in beats after the trigger that the transition will end.

**Duration to scale** -  the duration in beats, starting at the trigger for which notes will be captured

**Tempo multiplier** - this is the ratio of the tempo at the end of the transition to the tempo at the start.

Examples

End=12, Duration=12,Multiplier=2
In 4/4 notes are captured over 3 bars and compressed into 2 bars ending causing silence for the first bar, then after bar 2 to the end of bar 3 a steadily increasing speed to 2x.

End=32,Duration=24,Multiplier=0.5
In 4/4 notes are captured over 6 bars and expanded into 8 bars with steady slowing to half tempo.


## Random Delay
This one is pretty simple. Just offsets MIDI notes by a random period in millseconds between min and max parameters.

// Copyright 2021 Tony Butterfield
// MIT Licence
// for more information see https://gitlab.com/tonbut/logic-pro-scripter-scripts

var NeedsTimingInfo = true

var ACTIVE_NOTES = {}
var modWheel = 0
var minPitch = 12
var maxPitch = minPitch + 12 * 9
var pitchBendRange = 2

var pi = [-12, 12, 0, 0, 0, 0, 0, 0]
var pn = [4, 4, 0, 0, 0, 0, 0, 0]

var beatDivMin = 1
var beatDivMax = 64
var quantizeBeatDivision = false

var attackRate = 128 //0-128
var decayRate = 0 //-128-0
var sustainRate = 0
var releaseRate = -128
var sustainLevel = 0 //0-128
var stopOutOfRange = true
var silenceOutOfRange = false
var pitchSubdivision = 1
var pitchDifferential = 0
var timeDifferential = 0

function Sound (note, startTime, velocity) {

  this.velocity = velocity

  this.lastPitch = note * pitchSubdivision

  this.lastStep = 0
  this.pitchStep = -1
  this.pitchCount = 0
  this.lastOutputNote = null
  this.lastOutputBend = null
  this.lastNote = null
  this.lastModWheel = -1
  this.lastBeatDivision = 1
  this.pitchDifferentialOffset = 0
  this.stepDifferentialOffset = 0

  this.volume = 0
  this.volumePhase = 0 //0=attack, 1=decay, 2=sustain, 3=release, 4=end
  this.lastOutputVolume = null

  this.beatDivMinLog = Math.log(beatDivMin) / Math.log(2)
  this.beatDivMaxLog = Math.log(beatDivMax) / Math.log(2)
  this.updateBeatDivision()

  this.startTime = this.getQuantizedTime(startTime)
  this.lastTime = this.startTime
}

Sound.prototype.stop = function () {
  var cc = new NoteOff()
  cc.pitch = this.lastOutputNote
  cc.velocity = 0
  cc.sendAfterMilliseconds(1)

  var t = GetTimingInfo().blockStartBeat
  var duration = t - this.startTime
}

Sound.prototype.stopLast = function () {
  if (this.lastOutputNote != null) {
    var cc = new NoteOn()
    cc.pitch = this.lastOutputNote
    cc.velocity = 0
    cc.sendAfterMilliseconds(1)
  }
}

Sound.prototype.release = function () {
  this.volumePhase = 3
}

Sound.prototype.getQuantizedTime = function (v) {
  v += 1 / (this.lastBeatDivision * 4)
  fst = Math.floor(v)
  st = v - fst
  st = Math.floor(st * this.lastBeatDivision) / this.lastBeatDivision
  return fst + st
}

Sound.prototype.updateBeatDivision = function () {
  var x =
    (this.beatDivMinLog +
      (this.beatDivMaxLog - this.beatDivMinLog) * modWheel) /
    this.beatDivMaxLog
  var bd = Math.pow(2, this.beatDivMaxLog * x)
  if (quantizeBeatDivision) {
    e = Math.round(Math.log(bd) / Math.log(2))
    this.lastBeatDivision = Math.pow(2, e)
  } else {
    this.lastBeatDivision = bd
  }
}

Sound.prototype.process = function (info) {
  var stop = false
  var t = GetTimingInfo().blockStartBeat
  var duration = t - this.lastTime

  var step = Math.floor(duration * this.lastBeatDivision)
  if (step != this.lastStep) {
    if (quantizeBeatDivision) {
      this.lastTime = this.getQuantizedTime(t)
    } else {
      this.lastTime = t
    }

    this.lastStep = 0

    if (modWheel != this.lastModWheel) {
      this.lastModWheel = modWheel
      this.updateBeatDivision()
    }

    var pitch = this.lastPitch
    if (this.pitchStep >= 0) {
      pitch += pi[this.pitchStep]
      if (this.pitchStep == 0) pitch += this.pitchDifferentialOffset
      if (this.pitchStep == 1) pitch -= this.pitchDifferentialOffset
      if (this.pitchStep == 2) pitch += this.pitchDifferentialOffset

      this.pitchCount++
      if (this.pitchCount >= pn[this.pitchStep]) {
        this.pitchCount = 0

        var k = 0
        do {
          this.pitchStep++
          if (this.pitchStep >= pi.length) {
            this.pitchStep = 0
            this.pitchDifferentialOffset += pitchDifferential
            this.stepDifferentialOffset += timeDifferential
          }
          if (this.pitchStep == 0)
            this.pitchCount -= this.stepDifferentialOffset
          if (this.pitchStep == 1)
            this.pitchCount -= this.stepDifferentialOffset
          if (this.pitchStep == 2)
            this.pitchCount -= this.stepDifferentialOffset
        } while (pn[this.pitchStep] == 0 && k++ < 10)
      }
      if (pitch >= maxPitch * pitchSubdivision) {
        if (stopOutOfRange) this.volumePhase = 4
        else if (silenceOutOfRange) {
        } else pitch -= (maxPitch - minPitch) * pitchSubdivision
      } else if (pitch < minPitch * pitchSubdivision) {
        if (stopOutOfRange) this.volumePhase = 4
        else if (silenceOutOfRange) {
        } else {
          pitch += (maxPitch - minPitch) * pitchSubdivision
        }
      }
      this.lastPitch = pitch
    } else {
      this.pitchStep = 0
    }

    //process volume
    if (this.volumePhase == 0) {
      if (this.volume < 128) {
        this.volume += attackRate
      }
      if (this.volume >= 128) {
        this.volume = 128
        this.volumePhase = 1
      }
    } else if (this.volumePhase == 1) {
      if (this.volume > sustainLevel) {
        this.volume += decayRate
      } else {
        this.volumePhase = 2
        this.volume = sustainLevel
      }
    } else if (this.volumePhase == 2) {
      if (this.volume > 0) {
        this.volume += sustainRate
      } else {
        this.volumePhase = 4
      }
    } else if (this.volumePhase == 3) {
      if (this.volume > 0) {
        this.volume += releaseRate
      } else {
        this.volumePhase = 4
      }
    }

    if (this.volumePhase == 4 || this.volume <= 0) {
      this.stop()
      return true
    }

    var volume = Math.floor((this.volume * this.velocity) / 127)
    var note = Math.floor(pitch / pitchSubdivision)
    var bend = pitch % pitchSubdivision

    var volumeChanged = this.lastOutputVolume != volume
    var noteChanged = this.lastOutputNote != note
    var bendChanged = this.lastOutputBend != bend

    if (bendChanged) {
      this.lastOutputBend = bend
      var cc = new PitchBend()
      cc.value = (bend * 8192) / (pitchSubdivision * pitchBendRange)
      cc.send()
    }

    var outOfRange =
      this.lastPitch >= maxPitch * pitchSubdivision ||
      this.lastPitch < minPitch * pitchSubdivision
    if (volumeChanged || noteChanged) {
      if (!outOfRange) {
        var cc = new NoteOn()
        cc.pitch = note
        cc.velocity = volume
        cc.send()
        this.lastOutputVolume = volume
      }
    }

    if (noteChanged) this.stopLast()

    this.lastOutputNote = note
  }

  return stop
}

function ProcessMIDI () {
  var info = GetTimingInfo()
  var playing = info.playing
  var beat = info.blockStartBeat
  for (k in ACTIVE_NOTES) {
    if (ACTIVE_NOTES.hasOwnProperty(k)) {
      var stop = ACTIVE_NOTES[k].process(info)
      if (stop) {
        delete ACTIVE_NOTES[k]
      }
    }
  }
}

function HandleMIDI (event) {
  if (event instanceof NoteOn) {
    var op = event.pitch
    if (op < minPitch || op >= maxPitch) {
      return
    }

    for (k in ACTIVE_NOTES)
      if (ACTIVE_NOTES.hasOwnProperty(k)) {
        ACTIVE_NOTES[k].stop()
        delete ACTIVE_NOTES[k]
      }

    var info = GetTimingInfo()
    var velocity = event.velocity
    var sound = new Sound(op, info.blockStartBeat, velocity)
    ACTIVE_NOTES[op] = sound

  } else if (event instanceof NoteOff) {
    var op = event.pitch
    var sound = ACTIVE_NOTES[op]

    if (sound) {
      sound.release()
    }
  } else if (event instanceof ControlChange) {
    if (event.number == 1) {
      var val = event.value
      modWheel = val / 127
    }
  } else {
    event.send()
  }
}

var lastMaxPitchValue
function ParameterChanged (param, value) {
  switch (param) {
    case 1:
      minPitch = value * 12
      maxPitch = minPitch + lastMaxPitchValue * 12
      break
    case 2:
      lastMaxPitchValue = value
      maxPitch = minPitch + lastMaxPitchValue * 12
      break
    case 3:
      stopOutOfRange = value == 0
      silenceOutOfRange = value == 2
      break
    case 4:
      pitchSubdivision = value
      break

    case 6:
      beatDivMin = value
      break
    case 7:
      beatDivMax = value
      break
    case 8:
      quantizeBeatDivision = value == 0
      break
    case 10:
      attackRate = value
      break
    case 11:
      decayRate = -value
      break
    case 12:
      sustainLevel = value
      break
    case 13:
      releaseRate = -value
      break
    case 15:
      pitchDifferential = value
      break
    case 16:
      timeDifferential = value
      break
    default:
      if (param >= 18) {
        var i = param - 18
        var p = Math.floor(i / 2)
        var t = i % 2
        Trace(p + ' ' + t)
        if (t == 0) pi[p] = value
        else if (t == 1) pn[p] = value
      }
      break

  }
}

var PluginParameters = [
  {
    name: '------ Pitch Control ------',
    type: 'text'
  },
  {
    name: 'Low Octave',
    type: 'linear',
    minValue: 0,
    maxValue: 6,
    numberOfSteps: 6,
    defaultValue: 1
  },
  {
    name: 'Octave Range',
    type: 'linear',
    minValue: 1,
    maxValue: 10,
    numberOfSteps: 9,
    defaultValue: 8
  },
  {
    name: 'Out-of-range',
    type: 'menu',
    valueStrings: ['stop', 'wrap', 'silent'],
    minValue: 0,
    maxValue: 2,
    numberOfSteps: 2,
    defaultValue: 0
  },

  {
    name: 'Semitone subdivision',
    type: 'log',
    minValue: 1,
    maxValue: 4,
    numberOfSteps: 2,
    defaultValue: 1
  },
  {
    name: '------ Mod Wheel Speed ------',
    type: 'text'
  },
  {
    name: 'Beat Division Min',
    type: 'log',
    minValue: 1,
    maxValue: 64,
    numberOfSteps: 6,
    defaultValue: 8
  },
  {
    name: 'Beat Division Max',
    type: 'log',
    minValue: 1,
    maxValue: 64,
    numberOfSteps: 6,
    defaultValue: 64
  },
  {
    name: 'Quantize to ^2',
    type: 'menu',
    valueStrings: ['yes', 'no'],
    minValue: 0,
    maxValue: 1,
    numberOfSteps: 1,
    defaultValue: 0
  },

  {
    name: '------ ADSR ------',
    type: 'text'
  },
  {
    name: 'Attack',
    type: 'linear',
    minValue: 1,
    maxValue: 128,
    numberOfSteps: 127,
    defaultValue: 128
  },
  {
    name: 'Decay',
    type: 'linear',
    minValue: 0,
    maxValue: 128,
    numberOfSteps: 128,
    defaultValue: 0
  },
  {
    name: 'Sustain',
    type: 'linear',
    minValue: 0,
    maxValue: 128,
    numberOfSteps: 128,
    defaultValue: 0
  },
  {
    name: 'Release',
    type: 'linear',
    minValue: 0,
    maxValue: 128,
    numberOfSteps: 128,
    defaultValue: 128
  },
  {
    name: '------ Advanced Features ------',
    type: 'text'
  },
  {
    name: 'Pitch Step Differential',
    type: 'linear',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Time Step Differential',
    type: 'linear',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: '------ Pitch Pattern ------',
    type: 'text'
  },
  {
    name: 'Pitch 1',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 1',
    type: 'lin',
    minValue: 1,
    maxValue: 64,
    numberOfSteps: 63,
    defaultValue: 1
  },
  {
    name: 'Pitch 2',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 2',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 3',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 3',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 4',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 4',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 5',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 5',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 6',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 6',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 7',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 7',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  },
  {
    name: 'Pitch 8',
    type: 'lin',
    minValue: -12,
    maxValue: 12,
    numberOfSteps: 24,
    defaultValue: 0
  },
  {
    name: 'Steps 8',
    type: 'lin',
    minValue: 0,
    maxValue: 64,
    numberOfSteps: 64,
    defaultValue: 0
  }
]
